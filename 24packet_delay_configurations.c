/*************************************************************************
    > File Name: 24packet_delay_configurations.c
  > Author: wsun
  > Mail:sunweiflyus@gmail.com
  > Created Time: Wed 21 Dec 2016 11:22:46 PM CST
  > Comments: Packet delay models
 ************************************************************************/
#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>
#include <stdio.h>
static gsl_rng *r; //for using gsl lib

// for random delay model
enum
{
	UNIFORM = 1,
	TRUNORM,
	EXPONEN,
	SHIFGMA,
	WEIBULL,
	PARETO,
	MIX_TWO_SHIFGMA,
	MIX_TWO_NORMAL,
	MIX_TWO_WEIBULL
}   RANDOM_TYPE = MIX_TWO_SHIFGMA ; // which random model to select


int uniform()
{
	// All integers in the range [0,n-1] are produced with equal probability
	int tmp = gsl_rng_uniform_int(r, UNIFORM_RANGE) + 0.5; // int truancated

	return tmp;
}


int __normal(double mean, double stdv)
{
	//This function returns a Gaussian random variate, with mean zero and standard deviation sigma
	int tmp = gsl_ran_gaussian(r, stdv) + 0.5 + mean; // int truancated
	return  tmp;
}

int trunorm()
{
	int tmp = __normal(64, 100);

	if (tmp < 0)
		tmp = 0;
	else if (tmp > 32767)
		tmp = 32767;

	return  tmp;
}

#define EXPONEN_MEAN  100
int exponen()
{
	//This function returns a random variate from the exponential distribution with mean mu
	int tmp = gsl_ran_exponential(r, EXPONEN_MEAN) + 0.5;
	return tmp ;
}

double __shifgma(double a , double b, double shift)
{
	// Function: double gsl_ran_gamma (const gsl_rng * r, double a, double b)
	// a is shape, b is scale
	double res = gsl_ran_gamma(r, a, b) + shift; // int truancated
	return res;
}

int shifgma()
{
	int res = __shifgma(1.0, 2.5, 107.5) + 0.5 ;
	return  res;
}

int weibull()
{
	//Function: double gsl_ran_weibull (const gsl_rng * r, double a, double b)
	// a is scale, b is shape
	int tmp = gsl_ran_weibull (r, 1000, 1) + 0.5; // int truancated
	return  tmp;
}

int pareto()
{
	//Function: double gsl_ran_pareto (const gsl_rng * r, double a, double b)
	// a is shape, b is scale
	int tmp = gsl_ran_pareto (r, 0.85, 1) + 0.5; // int truancated
	return  tmp;
}


int mix_two_shifgma()
{
	int tmp = 0.2 * __shifgma(1, 2.5, 107.5) + 0.8 * __shifgma(1, 2.5, 7.5) + 0.5 ;
	return tmp;
}

int mix_two_normal()
{
	int tmp = 0.8 * __normal(64, 100) + 0.2 * __normal(1073741823, 10000) + 0.5;
	return tmp;
}

int mix_two_weibull()
{
	int tmp = 0.5 * gsl_ran_weibull(r, 1000, 1) + 0.5 * gsl_ran_weibull(r, 100, 1) + 0.5; // int truncated
	return  tmp;
}


int random_val()
{
	switch (RANDOM_TYPE)
	{
	case UNIFORM :
		return uniform();

	case TRUNORM:
		return trunorm();

	case EXPONEN:
		return exponen();

	case SHIFGMA:
		return shifgma();

	case WEIBULL:
		return weibull();

	case PARETO:
		return pareto();

	case MIX_TWO_SHIFGMA:
		return mix_two_shifgma();

	case MIX_TWO_NORMAL:
		return mix_two_normal();

	case MIX_TWO_WEIBULL:
		return mix_two_weibull();

	default:
		printf("wrong random generator type\n");
		exit(-1);
	}
}
